package com.pojo;

import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Liuxui entity. @author MyEclipse Persistence Tools
 */
@XmlRootElement
public class Liuxui implements java.io.Serializable {

	// Fields

	private Integer lxid;
	private String myself;
	private String taget;
	private Double money;
	private String state;
	private Timestamp time;

	// Constructors

	/** default constructor */
	public Liuxui() {
	}

	/** full constructor */
	public Liuxui(String myself, String taget, Double money, String state,
			Timestamp time) {
		this.myself = myself;
		this.taget = taget;
		this.money = money;
		this.state = state;
		this.time = time;
	}

	// Property accessors

	public Integer getLxid() {
		return this.lxid;
	}

	public void setLxid(Integer lxid) {
		this.lxid = lxid;
	}

	public String getMyself() {
		return this.myself;
	}

	public void setMyself(String myself) {
		this.myself = myself;
	}

	public String getTaget() {
		return this.taget;
	}

	public void setTaget(String taget) {
		this.taget = taget;
	}

	public Double getMoney() {
		return this.money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

}