package com.pojo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bank entity. @author MyEclipse Persistence Tools
 */
@XmlRootElement
public class Bank implements java.io.Serializable {

	// Fields

	private String card;
	private Double money;
	private String paypwd;

	// Constructors

	/** default constructor */
	public Bank() {
	}

	/** full constructor */
	public Bank(Double money, String paypwd) {
		this.money = money;
		this.paypwd = paypwd;
	}

	// Property accessors

	public String getCard() {
		return this.card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public Double getMoney() {
		return this.money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public String getPaypwd() {
		return this.paypwd;
	}

	public void setPaypwd(String paypwd) {
		this.paypwd = paypwd;
	}

}