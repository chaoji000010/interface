package com.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Users entity. @author MyEclipse Persistence Tools
 */
@XmlRootElement
public class Users implements java.io.Serializable {

	// Fields

	private Integer uid;
	private String uname;
	private String upwd;
	private String uaddress;
	private String utel;
	private String ustate;
	private String ucard;
	private Double yue;
	private String upaypwd;
	private Set uvcs = new HashSet(0);

	// Constructors

	/** default constructor */
	public Users() {
	}

	/** full constructor */
	public Users(String uname, String upwd, String uaddress, String utel,
			String ustate, String ucard, Double yue, String upaypwd, Set uvcs) {
		this.uname = uname;
		this.upwd = upwd;
		this.uaddress = uaddress;
		this.utel = utel;
		this.ustate = ustate;
		this.ucard = ucard;
		this.yue = yue;
		this.upaypwd = upaypwd;
		this.uvcs = uvcs;
	}

	// Property accessors

	public Users(String upwd, String ucard, String upaypwd) {
		super();
		this.upwd = upwd;
		this.ucard = ucard;
		this.upaypwd = upaypwd;
	}

	public Integer getUid() {
		return this.uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getUname() {
		return this.uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUpwd() {
		return this.upwd;
	}

	public void setUpwd(String upwd) {
		this.upwd = upwd;
	}

	public String getUaddress() {
		return this.uaddress;
	}

	public void setUaddress(String uaddress) {
		this.uaddress = uaddress;
	}

	public String getUtel() {
		return this.utel;
	}

	public void setUtel(String utel) {
		this.utel = utel;
	}

	public String getUstate() {
		return this.ustate;
	}

	public void setUstate(String ustate) {
		this.ustate = ustate;
	}

	public String getUcard() {
		return this.ucard;
	}

	public void setUcard(String ucard) {
		this.ucard = ucard;
	}

	public Double getYue() {
		return this.yue;
	}

	public void setYue(Double yue) {
		this.yue = yue;
	}

	public String getUpaypwd() {
		return this.upaypwd;
	}

	public void setUpaypwd(String upaypwd) {
		this.upaypwd = upaypwd;
	}

	public Set getUvcs() {
		return this.uvcs;
	}

	public void setUvcs(Set uvcs) {
		this.uvcs = uvcs;
	}

}