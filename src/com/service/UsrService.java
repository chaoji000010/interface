package com.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.domain.ReUser;
import com.pojo.Bank;
import com.pojo.Liuxui;
import com.pojo.Users;
import com.hibernate.HibernateSessionFactory;

@Path("userservice")
public class UsrService {
	@GET
	@Path("getuser/{ucard},{pwd}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ReUser> getUser(@PathParam("ucard")String ucard,@PathParam("pwd")String pwd){
		List<ReUser> user = new ArrayList<ReUser>();
		Session session = HibernateSessionFactory.getSession();
		String shql = "select new com.domain.ReUser(u.upwd,u.ucard,u.upaypwd) from Users u where u.ucard = ? and u.upwd = ? ";//
		Query q = session.createQuery(shql);
		q.setString(0, ucard);
		q.setString(1,pwd);
		user = (List<ReUser>)q.list();
//		Users u = new Users(user.get(0).getUname(), user.get(0).getUpwd());
		return user;
	}
	
	@GET
	@Path("addusercard/{ucard},{upwd},{upaypwd}")
	@Produces(MediaType.APPLICATION_JSON)
	public String addUser(@PathParam("ucard")String ucard,@PathParam("upwd")String upwd,@PathParam("upaypwd")String upaypwd){
		Session session = HibernateSessionFactory.getSession();
		System.out.println("执行了add");
		Transaction t = session.beginTransaction();
		System.out.println("执行了add=t");
		Users user = new Users(upwd, ucard, upaypwd);
		System.out.println(user.getUname()+"     " +user.getUpwd());
		try {
		      session.save(user);
		      t.commit();
		   } catch (Exception ef) { 
		       ef.printStackTrace(); 
		} 
		HibernateSessionFactory.closeSession();
		return "{\"adduser\":[{\"state\":\""+ 1 +"\"}]}";
	}
	/* 
	 * 支付：
	 * 1、获取用户余额，支付的钱数大于余额，支付失败，账号不对，交易失败，回滚
	 * 2、不大于时，用户余额减去支付钱数，明细表添加记录，企业增加钱数，明细表记录。
	 * 3、成功.失败时回滚
	 * 2、 */
	@GET
	@Path("pay/{ucard},{upaypwd},{taget},{dingdanid}")//已知用户账号，目标账号，支付密码，订单号
	@Produces(MediaType.APPLICATION_JSON)
	public String pay(@PathParam("ucard")String ucard,@PathParam("upaypwd")String upaypwd,@PathParam("taget")String taget,@PathParam("dingdanid")double dingdanid){
		List<Bank> bank = new ArrayList<Bank>();
		Session session = HibernateSessionFactory.getSession();
		Transaction t = session.beginTransaction();
		t.begin();
		String shql = "select new com.pojo.Bank from Bank b where b.card = ? and b.paypwd = ? ";//
		Query q = session.createQuery(shql);
		q.setString(0, ucard);
		q.setString(1,upaypwd);
		bank = (List<Bank>)q.list();
		if(bank.get(0) == null){
			System.out.println("用户账号错误");
			t.rollback();
			return "{\"pay\":[{\"state\":\""+ 1 +"\"}]}";//失败。。。。
		}
		double price = bank.get(0).getMoney();
		if(price < dingdanid){//余额小于订单的钱数。
			System.out.println("用户余额不足");
			t.rollback();
			return "{\"pay\":[{\"state\":\""+ 1 +"\"}]}";//失败。。。。
		}
		else if(price >= dingdanid){
			String hql = "update Bank b set b.moneoy = ? where b.card = ?";
			double newmoney = price - dingdanid;
			Query q1 = session.createQuery(hql);//用户账单修改
			q1.setDouble(0, newmoney);
			q1.setString(1, ucard);
			q1.setString(2, upaypwd);
			int r = q1.executeUpdate();
			if(r == 1){
				UsrService us = new UsrService();
				us.payliuxhui(ucard, taget, newmoney);
			}else{
				t.rollback();
			}
			//获取企业账号的余额
			List<Bank> b2 = new ArrayList<Bank>();
			String hql1 = "from  Bank b where  b.card = ?";
			Query q2 = session.createQuery(hql);//企业账单余额
			q2.setString(0, taget);
			b2 = q2.list();
			if(b2.size() == 0){
				System.out.println("企业账号错误");
				t.rollback();
				return "{\"pay\":[{\"state\":\""+ 1 +"\"}]}";//失败。。。。
			}
			double qiye = b2.get(0).getMoney();
			
			String hql3 = "update Bank b set b.money = ? where b.card = ?";
			Query q3 = session.createQuery(hql3);
			q3.setDouble(0, qiye + dingdanid);
			q3.setString(0, taget);
			int r2 = q3.executeUpdate();
			if(r2 != 1){
				t.rollback();
				return "{\"pay\":[{\"state\":\""+ 1 +"\"}]}";//失败。。。。
			}else{
				UsrService us = new UsrService();
				us.getliuxhui(taget, ucard, qiye + dingdanid);
			}
			t.commit();
			System.out.println("用户已支付");
		}
		HibernateSessionFactory.closeSession();
		return "{\"adduser\":[{\"state\":\""+ 1 +"\"}]}";
	}
	/* 
	 * 实行退款：
	 * 1、获取订单的交易额，用户账号，企业账号
	 * 2、不大于时，用户余额减去支付钱数，明细表添加记录，企业增加钱数，明细表记录。
	 * 3、成功.失败时回滚
	 * 2、 */
	
//	@GET        //公司id，公司支付密码，客户id，订单要付款的钱数
//	@Path("rebake/{ucard},{upaypwd},{taget},{dingdanid}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public String rebake(@PathParam("ucard")String ucard,@PathParam("upaypwd")String upaypwd,@PathParam("taget")String taget,@PathParam("dingdanid")double dingdanid){
//		List<Bank> bank = new ArrayList<Bank>();
//		Session session = HibernateSessionFactory.getSession();
//		Transaction t = session.beginTransaction();
//		t.begin();
//		String shql = "select new com.pojo.Bank from Bank b where b.card = ? and b.paypwd = ? ";//
//		Query q = session.createQuery(shql);
//		q.setString(0, ucard);
//		q.setString(1,upaypwd);
//		bank = (List<Bank>)q.list();
//		if(bank.get(0) == null){
//			System.out.println("用户账号错误");
//			t.rollback();
//			return "{\"pay\":[{\"state\":\""+ 1 +"\"}]}";//失败。。。。
//		}
//		double price = bank.get(0).getMoney();
//		if(price < dingdanid){//余额小于订单的钱数。
//			System.out.println("用户余额不足");
//			t.rollback();
//			return "{\"pay\":[{\"state\":\""+ 1 +"\"}]}";//失败。。。。
//		}
//		else if(price >= dingdanid){
//			String hql = "update Bank b set b.moneoy = ? where b.card = ?";
//			double newmoney = price - dingdanid;
//			Query q1 = session.createQuery(hql);//用户账单修改
//			q1.setDouble(0, newmoney);
//			q1.setString(1, ucard);
//			q1.setString(2, upaypwd);
//			int r = q1.executeUpdate();
//			if(r != 1){
//				
//				t.rollback();
//			}
//			//获取企业账号的余额
//			List<Bank> b2 = new ArrayList<Bank>();
//			String hql1 = "from  Bank b where  b.card = ?";
//			Query q2 = session.createQuery(hql);//企业账单余额
//			q2.setString(0, taget);
//			b2 = q2.list();
//			if(b2.size() == 0){
//				System.out.println("企业账号错误");
//				t.rollback();
//				return "{\"pay\":[{\"state\":\""+ 1 +"\"}]}";//失败。。。。
//			}
//			double qiye = b2.get(0).getMoney();
//			
//			String hql3 = "update Bank b set b.money = ? where b.card = ?";
//			Query q3 = session.createQuery(hql3);
//			q3.setDouble(0, qiye + dingdanid);
//			q3.setString(0, taget);
//			int r2 = q3.executeUpdate();
//			if(r2 != 1){
//				t.rollback();
//				return "{\"pay\":[{\"state\":\""+ 1 +"\"}]}";//失败。。。。
//			}
//			t.commit();
//			System.out.println("用户已支付");
//		}
//		HibernateSessionFactory.closeSession();
//		return "{\"adduser\":[{\"state\":\""+ 1 +"\"}]}";
//	}
//	
	
	public void payliuxhui(String userid,String taget ,double money){
		Session session = HibernateSessionFactory.getSession();
		Transaction t = session.beginTransaction();
		t.begin();
		Calendar calendar = Calendar.getInstance();
		Timestamp time = new Timestamp(calendar.getTimeInMillis());
		Liuxui lx = new Liuxui(userid, taget, money, "支出", time);
		session.save(lx);
		t.commit();
		HibernateSessionFactory.closeSession();
	}
	public void getliuxhui(String userid,String taget ,double money){
		Session session = HibernateSessionFactory.getSession();
		Transaction t = session.beginTransaction();
		t.begin();
		Calendar calendar = Calendar.getInstance();
		Timestamp time = new Timestamp(calendar.getTimeInMillis());
		Liuxui lx = new Liuxui(userid, taget, money, "收入", time);
		session.save(lx);
		t.commit();
		HibernateSessionFactory.closeSession();
	}
}
