package com.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReUser implements java.io.Serializable{
	private String upwd;
	private String ucard;
	private String upaypwd;
	
	public String getUcard() {
		return ucard;
	}




	public void setUcard(String ucard) {
		this.ucard = ucard;
	}




	public String getUpaypwd() {
		return upaypwd;
	}




	public void setUpaypwd(String upaypwd) {
		this.upaypwd = upaypwd;
	}








	public String getUpwd() {
		return upwd;
	}




	public void setUpwd(String upwd) {
		this.upwd = upwd;
	}







	public ReUser(String upwd, String ucard, String upaypwd) {
		super();
		this.upwd = upwd;
		this.ucard = ucard;
		this.upaypwd = upaypwd;
	}




	public ReUser() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
